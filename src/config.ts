function getBooleanValue(value: string): boolean {
  return value === 'true';
}
function toNumber(value: string): number {
  return +value;
}

export default {
  nodePort: toNumber(process.env.NODEPORT) || 3000,
  testing: getBooleanValue(process.env.TESTING),
  general: {
    namespace: process.env.NAMESPACE,
    username: process.env.USERNAME,
    uid: toNumber(process.env.UID),
  },
};

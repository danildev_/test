import { Injectable } from '@nestjs/common';
import config from './config';

@Injectable()
export class AppService {
  getHello(): string {
    return (
      '<h3 style="color: green">Result:</h3>Username: ' + config.general.username + '<br>' +
      'UID: ' + config.general.uid + '<br>' +
      'Namespace: ' + config.general.namespace + '<br>' +
      'Testing status: ' + config.testing
    );
  }
}
